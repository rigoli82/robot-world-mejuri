# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Depot.create([
  {
    name: 'Factory Stock',
    code: 'factory'
  }, {
    name: 'Store Stock',
    code: 'store'
  }, {
    name: 'Sold Cars',
    code: 'sold'
  }
])

Model.create([
  { name: 'Ka' },
  { name: 'Fiesta' },
  { name: 'Focus' },
  { name: 'Mondeo' },
  { name: 'Mustang' },
  { name: 'Galaxy' },
  { name: 'Ecosport' },
  { name: 'Explorer' },
  { name: 'Ranger' },
  { name: 'F-100' }
])