# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180810233040) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cars", force: :cascade do |t|
    t.integer  "model_id"
    t.integer  "depot_id"
    t.float    "price"
    t.float    "cost"
    t.integer  "year"
    t.string   "color"
    t.integer  "status",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "cars", ["depot_id"], name: "index_cars_on_depot_id", using: :btree
  add_index "cars", ["model_id"], name: "index_cars_on_model_id", using: :btree

  create_table "chasses", force: :cascade do |t|
    t.string   "serial_number"
    t.boolean  "defective"
    t.integer  "car_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "chasses", ["car_id"], name: "index_chasses_on_car_id", using: :btree

  create_table "computers", force: :cascade do |t|
    t.string   "serial_number"
    t.boolean  "defective"
    t.integer  "car_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "computers", ["car_id"], name: "index_computers_on_car_id", using: :btree

  create_table "depots", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "depots", ["code"], name: "index_depots_on_code", using: :btree

  create_table "engines", force: :cascade do |t|
    t.string   "serial_number"
    t.boolean  "defective"
    t.integer  "car_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "engines", ["car_id"], name: "index_engines_on_car_id", using: :btree

  create_table "lasers", force: :cascade do |t|
    t.string   "serial_number"
    t.boolean  "defective"
    t.integer  "car_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "lasers", ["car_id"], name: "index_lasers_on_car_id", using: :btree

  create_table "models", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "models", ["name"], name: "index_models_on_name", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "car_id"
    t.float    "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "orders", ["car_id"], name: "index_orders_on_car_id", using: :btree

  create_table "seats", force: :cascade do |t|
    t.string   "serial_number"
    t.boolean  "defective"
    t.integer  "car_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "seats", ["car_id"], name: "index_seats_on_car_id", using: :btree

  create_table "wheels", force: :cascade do |t|
    t.string   "serial_number"
    t.boolean  "defective"
    t.integer  "car_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "wheels", ["car_id"], name: "index_wheels_on_car_id", using: :btree

  add_foreign_key "cars", "depots"
  add_foreign_key "cars", "models"
  add_foreign_key "chasses", "cars"
  add_foreign_key "computers", "cars"
  add_foreign_key "engines", "cars"
  add_foreign_key "lasers", "cars"
  add_foreign_key "orders", "cars"
  add_foreign_key "seats", "cars"
  add_foreign_key "wheels", "cars"
end
