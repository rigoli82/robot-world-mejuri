class CreateDepots < ActiveRecord::Migration
  def change
    create_table :depots do |t|
      t.string :name
      t.string :code

      t.timestamps null: false
    end
    add_index :depots, :code
  end
end
