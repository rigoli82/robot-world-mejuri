class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.belongs_to :model, index: true, foreign_key: true
      t.belongs_to :depot, index: true, foreign_key: true
      t.float :price
      t.float :cost
      t.integer :year
      t.string :color
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
