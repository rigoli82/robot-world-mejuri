class CreateComputers < ActiveRecord::Migration
  def change
    create_table :computers do |t|
      t.string :serial_number
      t.boolean :defective
      t.belongs_to :car, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
