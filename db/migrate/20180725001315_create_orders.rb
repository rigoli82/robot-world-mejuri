class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :car, index: true, foreign_key: true
      t.float :amount

      t.timestamps null: false
    end
  end
end
