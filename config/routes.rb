Rails.application.routes.draw do
  get '/' => "dashboard#overview"

  get '/debug' => 'dashboard#debug'
  
  # Bot actions
  get '/builder-bot/build-car' => 'bots#builder_create_car'
  get '/builder-bot/build-cars' => 'bots#builder_create_cars'
  get '/builder-bot/reset' => 'bots#reset'
  get '/builder-bot/clear' => 'bots#clear'
  get '/guard-bot/move' => 'bots#guard_check_and_move_cars'
  get '/buy-bot/buy' => 'bots#buy_shop'
  get '/slack/test' => 'bots#slack_test'
  
  # root :to => "dashboard#overview"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
