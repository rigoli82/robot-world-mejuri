require 'net/http'
require 'uri'
require 'json'

class Bot
  
  def initialize
    @bot_name = self.class.to_s
    @filename = File.expand_path("log/#{@bot_name}.log", File.dirname(__FILE__))
  end
  
  # Message logger
  def log msg
    logger = Logger.new(@filename, 'daily')
    logger.info(msg)
    logger.close
  end
  
  def log_clear
    File.delete(@filename) if File.exist?(@filename)
  end
  
  def slack msg
    uri = URI.parse(Rails.configuration.slack_hook)
    
    message = "[#{@bot_name}]: #{msg}"
    header = {'Content-Type': 'application/json'}
    data = {'text': message}
    
    begin
    
      # Create the HTTP objects
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Post.new(uri.request_uri, header)
      request.body = data.to_json
      # Send the request
      response = http.request(request)

      log "Slack message sent: #{message}. (Code: #{response.code})"
      
      return true
    rescue => e
      log "Error sending slack message: #{e}"
      
      return false
    end
  end
end