class GuardBot < Bot
  
  def check_and_move_cars
    log "# Checking cars for defects..."
    factory = Depot.find_by code: 'factory'
    cars = factory.cars.completed.to_a
    
    total = cars.length
    
    log "Cars to analyze: #{cars.length}"
    
    if (cars.empty?) 
      log "No completed cars for checking/moving? Come on!"
      return
    end
    
    cars.select! do |car|
      defects = car.computer.diagnose
      
      
      next true if defects == false
      
      log "Checking car #{car.id}"
      
      if (defects.nil?)
        slack "Whoah! The computer for car #{car.id} is unresponsive. Unable to diagnose. Broken anyways. Disposal. **sad beep**"
      else 
        slack "Defects found in car #{car.id} in parts: #{defects}. Disposal."
      end
      
      car.status = :defective
      car.save
      
      next false
    end
    
    if (cars.empty?)
      log "All cars discarded. Human will be sad... **screwing head**"
      
      return
    end
    
    log "No defects on new cars! Long live Skynet! ehm.. I mean... human!" if cars.length == total
    
    log "Moving #{cars.length} cars to store warehouse."
    
    store = Depot.find_by code: 'store'
    cars.each do |car| 
      car.depot = store
      car.save
    end
  end
end