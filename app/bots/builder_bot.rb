class BuilderBot < Bot
  
  def build_cars(count = 1)
    log "### Building #{count} new #{count == 1 ? 'car' : 'cars'}..."

    count.times { build_car }
  end

  def build_car
    car = Car.new
    
    car.model = Model.random
    car.depot = Depot.find_by code: 'factory'
    car.year = 2000 + rand(18)
    car.cost = 15000 + rand(15000) + rand(99) / 100
    car.price = car.cost * 1.3
    car.status = :project
    car.color = 'None'
    
    car.save
    
    car = Factory.basic_structure_line(car) # First assembly line
    
    car = Factory.electronic_devices_line(car) # Second assembly line
    
    car = Factory.painting_and_final_details_line(car) # Third assembly line
    
    car.save
    
    log "New car completed: #{car}."
  end
  
  def clean_factory
    factory = Depot.find_by code: 'factory'
    count = factory.cars.length
    factory.cars.destroy_all
    
    log "#{count} factory cars recycled."
  end
  
  def clean_slate
    Car.destroy_all
  end
  
end