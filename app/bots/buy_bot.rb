class BuyBot < Bot
  
  def buy_car
    store = Depot.find_by code: 'store'
    model = Model.random
    car = store.cars.where(model: model).take
    
    if !car
      log "Unable to buy a #{model.name}. Out of stock."
      return false
    end
    
    order = Order.new
    order.amount = car.price
    order.car = car
    order.save
    
    car.depot = Depot.find_by code: 'sold'
    car.status = :sold
    car.save
    
    log "Car purchase succesful (##{order.id}) for $#{order.amount}: #{car}."
  end
end