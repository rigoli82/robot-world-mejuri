class Factory
  
  # Chassis, engine and wheels
  def self.basic_structure_line car
    4.times do
      car.wheels << Wheel.new
    end
    c = Chassis.create!
    car.chassis = c
    e = Engine.create!
    car.engine = e
    car.status = :stage1
    return car
  end
  
  # Laser and computer
  def self.electronic_devices_line car
    car.laser = Laser.create!
    car.computer = Computer.create!
    car.status = :stage2
    return car
  end
  
  # Seats and paint
  def self.painting_and_final_details_line car
    2.times do
      car.parts << Seat.new
    end
    car.color = Car::COLORS.sample # Get random color
    car.status = :completed
    return car
  end
  
end