class Car < ActiveRecord::Base
  belongs_to :model
  belongs_to :depot
  has_one :order, :dependent => :delete
  
  # parts
  has_many :wheels,   :dependent => :delete_all
  has_many :seats,    :dependent => :delete_all
  has_one :chassis,   :dependent => :delete
  has_one :laser,     :dependent => :delete
  has_one :computer,  :dependent => :delete
  has_one :engine,    :dependent => :delete

  # Validations  
  validates :wheels, length: { maximum: 4 }
  validates :seats, length: { maximum: 2 }
  
  COLORS = ['Red', 'Green', 'Blue', 'Black', 'White', 'Purple'].freeze
  
  def parts
    [wheels, seats, chassis, laser, computer, engine].flatten.compact
  end


  enum status: [:project, :stage1, :stage2, :completed, :sold, :defective]

  def to_s
    fields = []
    fields.push(self.year) if self.year
    fields.push(self.color) if self.color
    fields.push(self.model.name) if self.model
    fields.push("Cost: $%.2f" % self.cost) if self.cost
    fields.push("Price: $%.2f" % self.price) if self.price
    fields.push("[#{self.status}]") if self.status
    fields.push("At: #{self.depot.name}") if self.depot
    return fields.join(' | ')
  end

  def completed
    wheels.length == 4 &&
    chassis? &&
    laser? &&
    computer? &&
    engine? &&
    seats.length == 2 &&
    self.color? &&
    self.status == :completed
  end
end
