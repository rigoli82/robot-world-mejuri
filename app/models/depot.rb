class Depot < ActiveRecord::Base
  has_many :cars
  has_many :models, :through => :cars
end
