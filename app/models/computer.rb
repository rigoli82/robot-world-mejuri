class Computer < ActiveRecord::Base
  include CarPart
  belongs_to :car
  
  def diagnose
    return nil if self.defective # If the computer is broken then returns null (not responding)
    
    defects = self.car.parts.select {|p| p.defective?}
    
    return false if defects.empty?
    
    return defects.map {|part| "#{part.class.to_s} S/N: #{part.serial_number}" }.join(' | ')
  end  
end
