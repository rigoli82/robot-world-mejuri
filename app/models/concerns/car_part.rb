module CarPart
  extend ActiveSupport::Concern
  
  included do
    after_initialize :generate_serial_number
    after_initialize :generate_defect
    
  end
  
  def generate_serial_number
    return if !self.new_record?
    
    loop do
      self.serial_number = SecureRandom.hex(10)
      break unless self.class.exists?(:serial_number => self.serial_number)
    end
  end
  
  def generate_defect
    return if !self.new_record?
    
    self.defective = (rand() > 0.95)
  end
  
end