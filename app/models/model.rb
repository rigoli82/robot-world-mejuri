class Model < ActiveRecord::Base
  def self.random
    self.limit(1).order("RANDOM()").first
  end
end
