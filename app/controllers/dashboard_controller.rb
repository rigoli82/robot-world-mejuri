class DashboardController < ApplicationController
  def overview
  end

  def debug
    @models = Model.all
    @depots = Depot.all
    @cars = Car.all
    @orders = Order.all
    
    @parts = [
      {part: 'Wheels', count: Wheel.all.length},
      {part: 'Seats', count: Seat.all.length},
      {part: 'Computers', count: Computer.all.length},
      {part: 'Laser', count: Laser.all.length},
      {part: 'Chasses', count: Chassis.all.length},
      {part: 'Engines', count: Engine.all.length},
    ]
    
    log_path = "#{File.dirname(__FILE__)}/../bots/log"
    
    @builder_log = File.exist?(filename = "#{log_path}/BuilderBot.log") ? File.readlines(filename) : nil
    @guard_log = File.exist?(filename = "#{log_path}/GuardBot.log") ? File.readlines(filename) : nil
    @buy_log = File.exist?(filename = "#{log_path}/BuyBot.log") ? File.readlines(filename) : nil
  end
end
