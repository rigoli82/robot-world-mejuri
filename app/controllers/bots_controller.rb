class BotsController < ApplicationController

  def builder_create_cars
    BuilderBot.new.build_cars(10)
    
    redirect_to :back
  end
  
  def builder_create_car
    BuilderBot.new.build_cars
    
    redirect_to :back
  end
  
  def reset
    # Delete cars from factory
    BuilderBot.new.clean_factory

    redirect_to :back
  end
  
  def clear
    # Delete all cars
    BuilderBot.new.clean_slate
    
    #Clear all logs
    BuilderBot.new.log_clear
    GuardBot.new.log_clear
    BuyBot.new.log_clear
    
    redirect_to :back
  end

  def guard_check_and_move_cars
    GuardBot.new.check_and_move_cars
    
    redirect_to :back
  end

  def slack_test
    BuilderBot.new.slack('This sentence is false.')
    
    redirect_to :back
  end
  
  def buy_shop
    BuyBot.new.buy_car
    
    redirect_to :back
  end

end
