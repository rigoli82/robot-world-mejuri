require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  test "should get overview" do
    get :overview
    assert_response :success
  end

  test "should get debugger" do
    get :debugger
    assert_response :success
  end

end
